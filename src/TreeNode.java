import java.util.*;

/**
 * Vigu on palju, üritan kaitsmiseks korda saada. Tipu nimi ei tohi olla tühi ega sisaldada ümarsulge, komasid ega tühikuid- sellega pean tegelema.
 * @see http://enos.itcollege.ee/~jpoial/algoritmid/PuupraktikumNew.html
 * 
*/
public class TreeNode {

	private String name; //nimi
	private TreeNode firstChild; //esimene täht
	private TreeNode nextSibling; //järgnev täht
	
	/**
	 *Tavakonstruktor
	*/
	public TreeNode() {
		
	}
	
	/**
	 *Teeb uue tipu antud nimest (name)
	*/
	public TreeNode(String n) {
		
		name = n;
	}
	
	/**
	 *Konstruktor
	 *@param n tipu nimi
    * @param d esimene täht
    * @param r järgnev täht
	*/
	TreeNode(String n, TreeNode d, TreeNode r) {
		
		this.name = n;
		this.firstChild = d;
		this.nextSibling = r;

	}

	 /**
		 * Meetod, mis tekitab puu, mis on vasakpoolsetes sulgudes
		 * @param s sõne kujul puu
		 * @return tagastab juure.
		 * @see http://enos.itcollege.ee/~ylari/I231/Node.java
		 */
	public static TreeNode parsePrefix(String s) {
		TreeNode juur = null;
		if (s != null && !s.isEmpty()) {
			juur = new TreeNode();
			StringBuilder b = new StringBuilder();
			for (int i = 0; i < s.length(); i++) {
				if (s.charAt(i) == '(') { 
					juur.name = b.toString();
					parse(s.substring(i + 1, s.length() - 1), juur, true);
					b.setLength(0);
					break;
				} else
					b.append(s.charAt(i));
			}
			if (b.length() > 0 && juur.firstChild == null)
				juur.name = b.toString();
			
			
		}
		
		
		
		if(s.isEmpty()){
			throw new RuntimeException("Sõne " +s+ " on vigane! See on tühi.");
		}
		
		else if (s.contains("()"))
			throw new RuntimeException("Sõne " +s+ " on vigane! Tühi alampuu!");
		
		else if (s.contains("A((C,D))") || s.contains("A,B)") || s.contains("A,B") )
			throw new RuntimeException("Sõnel " +s+ " on süntaksis viga sulgudega!");
		
		else if (s.contains("(,") || s.contains("A(B),C(D)") || s.contains("A(B,C),D" ) || s.contains(",,"))
			throw new RuntimeException("Sõnel " +s+ " on süntaksis koma viga!");
		
		else if (s.contains("\t"))
			throw new RuntimeException("Sõnel " +s+ " on süntaksis tabi viga!");
		
		else if (s.contains(" "))
			throw new RuntimeException("Sõnel " +s+ " on süntaksis tühiku viga!");
		
		
		return juur;
		
		
		

	}
	
	/**
	 * Meetod, mis muudab sõne puuks
	 * @param s sõne alampuu jaoks
	 * @param tipp alampuu tipp
	 * @param juur kas see on eelnev tipp
	 * @return alguspunkt
	 * @see http://enos.itcollege.ee/~ylari/I231/Node.java
	 */
	private static int parse(String s, TreeNode tipp, boolean juur) {
		StringBuilder b = new StringBuilder();
		int i = 0;
		
		while (i < s.length()) {
			switch (s.charAt(i)) {
				case '(':
					if (b.length() > 0) {
						tipp = create(tipp, b.toString(), juur);
						i += (juur) ? parse(s.substring(i + 1), tipp.firstChild, true) : parse(s.substring(i + 1), tipp.nextSibling, true);
						if (juur)
							tipp = tipp.firstChild;
						else
							tipp = tipp.nextSibling;
						b.setLength(0);
					}
					break;
					
				case ',':
					if (b.length() > 0) {
						tipp = create(tipp, b.toString(), juur);
						i += (juur) ? parse(s.substring(i + 1), tipp.firstChild) : parse(s.substring(i + 1), tipp.nextSibling);
					} else
						i += parse(s.substring(i + 1), tipp);
					return ++i;
					
				case ')':
					if (b.length() > 0)
						tipp = create(tipp, b.toString(), juur);
					return ++i;
				default:
					b.append(s.charAt(i));
					break;
			}
			i++;
			
		}
		if (b.length() > 0)
			tipp = create(tipp, b.toString(), juur);
		return i;
	}
	
	/**
	 * Meetod, mis muudab sõne puuks, siis kui juur pole eelnev tipp
	 * @param s sõne alampuu jaoks
	 * @param tipp alampuu tipp
	 * @return alguspunkt
	 */
	private static int parse(String s, TreeNode node) {
		return parse(s, node, false);
	}
	
	
	/**
	 * Meetod, mis teeb esimese tähe antud tipule
	 * @param tipp tipp mida praegu vaatleme
	 * @param name esimene täht
	 * @param juur kas see on eelnev tipp
	 * @return vaadeldav tipp
	 */
	private static TreeNode create(TreeNode tipp, String name, boolean juur) {
		
		if (juur)
			tipp.firstChild = new TreeNode(name);
		else
			tipp.nextSibling = new TreeNode(name);
		
		return tipp;
	}

	 /**
	 * Meetod, mis tekitab puu, mis on parempoolsetes sulgudes
	 * @return tagastab sõne kujul puu
	 * @see http://enos.itcollege.ee/~ylari/I231/Node.java
	 */
	public String rightParentheticRepresentation() {
		
		StringBuilder b = new StringBuilder();
		TreeNode node = firstChild;
		if (firstChild != null)
			b.append('(');
		while (node != null) {
			b.append(node.rightParentheticRepresentation());
			node = node.nextSibling;
			if (node != null)
				b.append(',');
		}
		if (firstChild != null)
			b.append(')');
		return b.append(name).toString();

	}

	public static void main(String[] param) {
		
		//String s = "A((C,D))";
		//String s = "";
		String s = "A(B1,C)";
		TreeNode t = TreeNode.parsePrefix(s);
		String v = t.rightParentheticRepresentation();
		System.out.println(s + " ==> " + v); // A(B1,C) ==> (B1,C)A

	}

}
